# Interface and interaction design Project Design B

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Dockerize
```
docker build -t iid-design-b .
docker run -it -p 8080:80 --rm --name iid-design-b-1 iid-design-b
```

### Podman
```
podman build -t iid-design-b .
podman run -it -p 8080:80 --rm --name iid-design-a-1 iid-design-b
```